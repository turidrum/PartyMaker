# Party Maker

Python version of the old [PartyMaker](legacy/PartyMaker.html) with export for pastebin.


## License

This program is free software and released under [GPLv2](LICENSE.md).
