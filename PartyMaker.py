#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#
# ------------------------------------------------------------------------------
#
#   PartyMaker - Python version of the old PartyMaker with export for pastebin
#   Copyright (C) 2018 turidrum
#
#   This program is free software; you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation; either version 2 of the
#   License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
#   02110-1301, USA.
#
# ------------------------------------------------------------------------------




import sys, os
import tkinter as tk
from tkinter import ttk
import configparser
import datetime
from time import sleep
import re

from gui import Gui
from importer import Character, SubscriptionList, SpreadsheetInput, SpreadsheetImporter
from partytable import PartyTable
from exporter import Exporter

class FPMaker:
    def __init__(self, configFile="config.ini"):
        """Constructor

        """
        config = configparser.ConfigParser()
        config.read(configFile)
        
        root = tk.Tk()
        self.gui = Gui(root)
        self.gui.master.title("PartyMaker")
        self.gui.menuFile.add_command(label="Open", command=self.gui.openFile)
        self.gui.menuFile.add_command(label="Save", command=self.gui.saveFile)
        self.gui.menuFile.add_command(label="Import from spreadsheet", command=self.importSpreadsheet)
        self.gui.menuFile.add_command(label="Export", command=self.export)
        self.gui.menuFile.add_separator()
        self.gui.menuFile.add_command(label="Exit", command=self.stop)
        self.gui.pack(side="left", fill="both", expand=True)
        self.subscriptionList = SubscriptionList()
        self.spreadsheetInput = SpreadsheetInput()
        self.spreadsheetImporter = SpreadsheetImporter(config["spreadsheet"]["URL"])
        self.partyTable = PartyTable(membersPerParty=4, passLength=5)
        self.exporter = Exporter(developerAPIKey=config["pastebin.com"]["DeveloperAPIKey"], expireDate=config["pastebin.com"]["ExpireDate"])
        self.availablePartyNames = [ # https://en.wikipedia.org/wiki/NATO_phonetic_alphabet
            "Alpha",
	    "Bravo",
	    "Charlie",
	    "Delta",
	    "Echo",
	    "Foxtrot",
	    "Golf",
	    "Hotel",
	    "India",
	    "Juliett",
	    "Kilo",
	    "Lima",
	    "Mike",
	    "November",
	    "Oscar",
	    "Papa",
	    "Quebec",
	    "Romeo",
	    "Sierra",
	    "Tango",
	    "Uniform",
	    "Victor",
	    "Whiskey",
	    "X-ray",
	    "Yankee",
	    "Zulu"
        ]
        self.members = []
        self.mergedMembers = []
        self.gui.mergeButton.config(command=self.parseMembers)
        self.gui.partyEditor.makePartiesButton.config(command=self.makeTables)
        
    def start(self):
        """Main loop

        """
        self.gui.mainloop()


    def stop(self):
        """Quit

        """
        self.gui.statusbar["text"] = "Goodbye!"
        sleep(1)
        self.gui.quit()
        exit(0)
    
    
    
    def openFile(self):
        msg = """An Error occurred:\n\nImpossible to open a file, this function is not yet implemented.
        """
        self.gui.mbox.showerror(title="Error", message=msg)
        
    def saveFile(self):
        msg = """An Error occurred:\n\nImpossible to save the file, this function is not yet implemented.
        """
        self.gui.mbox.showerror(title="Error", message=msg)

    def importSpreadsheet(self):
        self.gui.statusbar["text"] = "Importing data from spreadsheet..."
        self.gui.update()
        self.gui.spreadsheetInput.editText(method="erase")
        self.gui.spreadsheetInput.editText(self.spreadsheetImporter.fetch())
        self.gui.statusbar["text"] = "Spreadsheet data imported."
        
    
    def export(self):
        self.gui.statusbar["text"] = "Exporting to pastebin.org..."
        self.gui.update()
        title = "Party Table {}".format(datetime.datetime.utcnow().strftime("%Y-%m-%d"))
        
        contentsText = self.gui.parties.getContents()
        responseSuccess = self.exporter.export(paste=contentsText, title=title)
        if responseSuccess:
            message = "Export Party Table complete, link: {}".format(responseSuccess[2])
        else:
            message = "Something went wrong...({})".format(self.exporter.response)
        self.gui.links.editText("\n--------------------------------------------------------------------------------\n")
        self.gui.links.editText(message, method="add")
        self.gui.statusbar["text"] = message
        
        contentsSpreadsheet = self.gui.spreadsheetOutput.getContents()
        responseSuccess = self.exporter.export(paste=contentsSpreadsheet, title=title)
        if responseSuccess:
            message = "Export Spreadsheet complete, link: {}".format(responseSuccess[1])
        else:
            message = "Something went wrong...({})".format(self.exporter.response)
        self.gui.links.editText("\n--------------------------------------------------------------------------------\n")
        self.gui.links.editText(message, method="add")
        self.gui.statusbar["text"] = message
        
        

    def parseMembers(self):
        """Parses the subscription list and the spreadsheet input, makes a
        list of members and puts their names in the party editor

        """

        self.gui.statusbar["text"] = "Reading subscription list and spreadsheet input..."
        subscribed = self.subscriptionList.parse(self.gui.subscriptionList.getContents())
        columns = self.spreadsheetInput.parse(self.gui.spreadsheetInput.getContents())
        
        self.mergedMembers = []
        c = 0                   # counter for the subscribed
        for sub in subscribed:
            columnFound = False
            for col in columns:
                if sub["ign"].lower() == col["ign"].lower():
                    doSession = True
                    columnFound = True
                    today = datetime.datetime.now()
                    try:
                        columnDate = datetime.datetime.strptime("{}/{}".format(today.year, col["update"]), "%Y/%m/%d, %H:%M %p")
                    except ValueError: # pay attention, this is because brutal's alts don't have a timer in their column...
                        columnDate = today

                    columnDate.replace(year = today.year)
                    if today < columnDate:
                        # this must be the past year
                        columnDate = datetime.datetime.strptime("{}/{}".format(today.year-1, col["update"]), "%Y/%m/%d, %H:%M %p")
                        # check that last update is not too old
                    if (today - columnDate).days > 14:
                        col["spreadsheetStatus"] = "column too old"
                        doSession = False
                    else:
                        col["spreadsheetStatus"] = "ok"
                    member = Character(
                        ign=sub["ign"],
                        level=col["level"],
                        dps=col["dps"],
                        yc=sub["yc"],
                        ycStatus=sub["status"],
                        ycReason=sub["reason"],
                        partyRole=sub["role"],
                        subscribed=sub["subscribed"],
                        spreadsheetStatus=col["spreadsheetStatus"],
                        info=sub["info"]
                    )
                    
                    subscribed[c]["spreadsheet"] = member.spreadsheetStatus                    
                    
                    # check for subscription
                    if member.subscribed.lower() == "no":
                        doSession = False
                    # check for penalty session
                    penalty = re.match("penalty session", member.ycStatus.lower())
                    if penalty:
                        doSession = False
                    
                    if doSession or subscribed[c]["forceIn"].lower() == "yes":
                        self.mergedMembers.append(member)
            if columnFound == False:
                subscribed[c]["spreadsheet"] = "column not found"
                # force the member to do the session even if has no column
                if subscribed[c]["forceIn"].lower() == "yes" and subscribed[c]["forceDps"] != "" and subscribed[c]["forceLevel"] != "":
                    member = Character(
                        ign=subscribed[c]["ign"],
                        level=subscribed[c]["forceLevel"],
                        dps=subscribed[c]["forceDps"],
                    )
                    self.mergedMembers.append(member)
            c += 1
        self.gui.statusbar["text"] = "Updating subscription list..."
        self.gui.subscriptionList.editText(method="erase")
        self.subscriptionList.update(subscribed)
        self.gui.subscriptionList.editText(self.subscriptionList.makeList())
        self.mergedMembers = sorted(self.mergedMembers, key=lambda m: m.dps, reverse=True)
        self.members = [x.ign for x in self.mergedMembers]
        self.gui.partyEditor.makeTable(members=self.members, availablePartyNames=self.availablePartyNames)
        self.gui.statusbar["text"] = "Party editor ready."
        
        
    def makeTables(self):
        """Makes the party tables(text table and spreadsheet table)
        accordingly to how the parties are in the party editor
        
        """

        self.gui.statusbar["text"] = "Reading data from party editor..."
        self.partyTable.parties = []
        editorParties = self.gui.partyEditor.getParties()
        for i in range(0, len(editorParties)):
            party = editorParties[i]["party"]
            members = []
            # the editor returns a list of member names, the other
            # data was previously kept in self.mergedMembers, so use
            # the editor list only as reference for names
            for m in editorParties[i]["members"]:
                members.append(next((x for x in self.mergedMembers if x.ign == m), Character(ign="Unknown")))
            self.partyTable.addParty(party, members)
        self.gui.parties.editText(method="erase")
        self.gui.spreadsheetOutput.editText(method="erase")
        self.gui.parties.editText(self.partyTable.getTableTextMode())
        self.gui.spreadsheetOutput.editText(self.partyTable.getTableSpreadsheetMode())
        self.gui.statusbar["text"] = "Party tables completed."


if __name__ == "__main__":
    if len(sys.argv) > 1:
        for arg in zip(sys.argv[1::2], sys.argv[2::2]):
            if arg[0] == "--config" or arg[0] == "-c":
                app = FPMaker(arg[1])
    else:
        app = FPMaker()
    app.start()

