#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#
# ------------------------------------------------------------------------------
#
#   PartyMaker - Python version of the old PartyMaker with export for pastebin
#   Copyright (C) 2018 turidrum
#
#   This program is free software; you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation; either version 2 of the
#   License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
#   02110-1301, USA.
#
# ------------------------------------------------------------------------------




import tkinter as tk
from tkinter import ttk


class Banner(ttk.Frame):
    
    def __init__(self, parent, *args, **kwargs):
        """
        Constructor
        """
        ttk.Frame.__init__(self, parent, *args, **kwargs)
        self.parent = parent
        style = ttk.Style()
        style.configure("banner.TFrame", background="#333333")
        self.config(style="banner.TFrame")

    
    def loadImage(self, path):
        img = tk.PhotoImage(file=path)
        label = ttk.Label(self, image=img)
        label.config(relief="flat", borderwidth=0)
        self.img = img
        self.label = label
        self.label.pack()
        
        

if __name__ == "__main__":
    root = tk.Tk()
    b = Banner(root)
    b.pack(side="top", fill="both", expand=True)
    b.loadImage("assets/hwf-logo.png")
    
    
    root.mainloop()
