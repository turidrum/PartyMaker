#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#
# ------------------------------------------------------------------------------
#
#   PartyMaker - Python version of the old PartyMaker with export for pastebin
#   Copyright (C) 2018 turidrum
#
#   This program is free software; you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation; either version 2 of the
#   License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
#   02110-1301, USA.
#
# ------------------------------------------------------------------------------



import math
import tkinter as tk
from tkinter import ttk


class PartyEditor(ttk.Frame):
    def __init__(self, parent, *args, **kwargs):
        """
        Constructor
        """
        ttk.Frame.__init__(self, parent, *args, **kwargs)
        
        self.switchStack = []
        self.tableMembers = []
        
        self.columnconfigure(0, weight=1)
        self.rowconfigure(0, weight=1)
        self.parent = parent
        self.tableContainer = ttk.Frame(self)
        self.tableContainer.grid(row=0, column=0, columnspan=1, sticky="WENS")
        self.master.bind_class("Text","<Control-KeyRelease-a>", self.selectAll)
        self.tableContainer.grid(column=0, row=0, columnspan=1, sticky="NSWE")
        self.makePartiesButton = ttk.Button(self, text="Make parties")
        self.makePartiesButton.grid(row=1, column=0, columnspan=1, sticky="WE")
        headerstyle = ttk.Style()     # header
        headerstyle.configure("Header.TLabel", foreground="#f0f0f0", background="#007acd", font=("Helvetica", 14, "bold"))
        self.availablePartyNames = []
        self.parties = []
        
        
    
    def makeTable(self, members=[], membersPerParty=4, availablePartyNames=[]):
        """
        Makes a table representing the parties

        @param members                          a list of member names
        @param membersPerParty                  the number of members for each party
        """
        self.availablePartyNames = availablePartyNames
        self.parties = []
        
        for widget in self.tableContainer.winfo_children():
            widget.destroy()
        self.tableMembers = []
        
        header = ["Party"]
        for p in range(0, membersPerParty):
            header.append("Player {}".format(p+1))
        
        numParties = math.ceil(len(members)/membersPerParty)
        
        for row in range(0, numParties+1): # 0 is the header, so add 1
            self.tableContainer.rowconfigure(row, weight=1)
            if row == 0:
                entries = header
            else:
                entries = [self.availablePartyNames[row-1]]
                self.parties.append({"party": self.availablePartyNames[row-1], "members": []})
                c = 1           # unknown counter, needed to avoid problems with non unique names
                for p in range(0, membersPerParty):
                    if row == numParties and len(members) % membersPerParty > 0 and p >= len(members) % membersPerParty:
                        member = "Unknown-{}".format(c)
                        c += 1
                    else:
                        member = members[((row-1)*membersPerParty)+p]
                    entries.append(member)
                    self.parties[-1]["members"].append(member)
            for col in range(0, len(entries)):
                if row == 0 or col == 0:
                    field = ttk.Label(self.tableContainer, text=entries[col], anchor=tk.CENTER, relief="sunken", style="Header.TLabel")
                else:
                    field = ttk.Button(self.tableContainer, text=entries[col], command=(lambda m=entries[col]: self.slideMember(m)))
                    self.tableMembers.append(entries[col])
                
                field.grid(row=row, column=col, columnspan=1, sticky="WENS")
                self.tableContainer.columnconfigure(col, weight=1)
            
        
    
    def getParties(self):
        return self.parties

    def slideMember(self, member):
        """Moves a member from one position to another, and shift all the
        members in between by one position, it must be invoked two
        times to track the source and destination

        @param member                           the name of the member to move or to be moved in its place

        """
        if len(self.switchStack) == 0:
            self.switchStack.append(member)
        else:
            self.switchStack.append(member)
            switchFrom = self.tableMembers.index(self.switchStack[0])
            switchTo = self.tableMembers.index(self.switchStack[1])
            stack = []
            if switchFrom != switchTo:
                if switchFrom < switchTo:
                    stackBegin = self.tableMembers[0:switchFrom]
                    stackModified = self.tableMembers[switchFrom+1:switchTo+1]
                    stackModified.append(self.tableMembers[switchFrom])
                    stackEnd = self.tableMembers[switchTo+1:]
                    stack.extend(stackBegin)
                    stack.extend(stackEnd)
                    stack = stackBegin + stackModified + stackEnd
                else:
                    stackBegin = self.tableMembers[0:switchTo]
                    stackModified = self.tableMembers[switchTo:switchFrom]
                    stackModified.insert(0, self.tableMembers[switchFrom])
                    stackEnd = self.tableMembers[switchFrom+1:]
                    stack.extend(stackBegin)
                    stack.extend(stackEnd)
                    stack = stackBegin + stackModified + stackEnd
                
                self.makeTable(stack, availablePartyNames=self.availablePartyNames)
            self.switchStack = []
    
        
    def selectAll(self, event):
        """
        selects the text in a widget
        
        @param event                    tk event
        """
        
        event.widget.tag_add("sel","1.0","end")

    def editText(self, text = "", method = "add"):
        """
        edit the Text widget in two modes, add and erase
        
        @param text                     the text to be edit
        @param method                   type of edit, can be add(default) or erase
        """
        
        target = self.text
        if method == "erase":
            target.delete(1.0, tk.END)
        elif method == "add":
            target.insert(tk.INSERT, "%s\n" % text)

    def getContents(self):
        """
        Retrieves the contents of the text widget
        
        @return                         string representing the contents of the widget
        """

        return self.text.get(1.0, tk.END)
        

if __name__ == "__main__":
    root = tk.Tk()
    pe = PartyEditor(root)
    pe.pack(side="top", fill="both", expand=True)
    availablePartyNames = [ # https://en.wikipedia.org/wiki/NATO_phonetic_alphabet
        "Alpha",
	"Bravo",
	"Charlie",
	"Delta",
	"Echo",
	"Foxtrot",
	"Golf",
	"Hotel",
	"India",
	"Juliett",
	"Kilo",
	"Lima",
	"Mike",
	"November",
	"Oscar",
	"Papa",
	"Quebec",
	"Romeo",
	"Sierra",
	"Tango",
	"Uniform",
	"Victor",
	"Whiskey",
	"X-ray",
	"Yankee",
	"Zulu"
    ]

    pe.makeTable([
        "OmegaMan5000",
        "Modeon",
        "brutalboy",
        "brutal's-alt-1",
        "brutal's-alt-2",
        "Damador",
        "bmerich33",
        "Timonius",
        "Lyve",
        "Baga",
        "xDx",
        "anonymoous",
        "Jelle",
        "Rdbj",
        "Faaaaaart",
        "dmwro",
        "daninin",
        "OldBarbon",
        "DanyX94",
        "Xfxforce",
        "Aperikub",
        "Tiopa",
        "Acmano179",
        "monoxxide",
        "Jumpz",
        "simbu",
        "locker01",
        "aethnight",
        "iforgiveyou",
        "QContinuum",
        "xBlo",
        "Iluya"
    ], availablePartyNames)
    root.mainloop()
