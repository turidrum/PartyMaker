#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#
# ------------------------------------------------------------------------------
#
#   PartyMaker - Python version of the old PartyMaker with export for pastebin
#   Copyright (C) 2018 turidrum
#
#   This program is free software; you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation; either version 2 of the
#   License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
#   02110-1301, USA.
#
# ------------------------------------------------------------------------------


import random



class PartyTable:
    
    def __init__(self, membersPerParty=4, passLength=5):
        """Constructor

        @param membersPerParty                  the number of members in each party
        @param passLength                       the length of the passwords for the parties

        """
        
        
        self.membersPerParty = membersPerParty
        self.passLength = passLength
        self.parties = []
        
    
    def addParty(self, party, members):
        """Adds a party to the table

        @param name                             name of the party
        @param members                          list of members and their dps
        
        """
        
        self.parties.append({
            "party": party,
            "password": self.makePassword(self.passLength),
            "members": members,
            "average": int(sum([x.dps for x in members if x.dps > 0])/len([x.dps for x in members if x.dps > 0])) # accepting
                                                                                                                  # dps
                                                                                                                  # greater
                                                                                                                  # than
                                                                                                                  # zero
                                                                                                                  # avoid
                                                                                                                  # to
                                                                                                                  # count
                                                                                                                  # unknown
                                                                                                                  # members
                                                                                                                  # in
                                                                                                                  # this
                                                                                                                  # average
        })


    def makePassword(self, passLength):
        """Simple password maker

        @param passLength                       length of the password
        @return                                 the password
        
        """
        
        alphabet = "0123456789ABCDEF"
        password = "".join([random.choice(alphabet) for x in range(0,passLength)])
        return password
        
        
    
    def getTableTextMode(self):
        """Makes the table in plain text
        
        @return                                 string containing the table

        """
        
        padding = 1
        table = ""
        rowSeparator = "+"
        
        # headers and the minimum required width for their columns
        headers = [["Party",0], ["Password",0]]
        for p in range(0, self.membersPerParty):
            headers.append(["Player {}".format(p+1),0])
        headers.append(["Average",0])
        
        for i in range(0, len(headers)):
            if i == 0:          # party and password are strings
                headers[i][1] = max(len(headers[i][0]), len(str(max(self.parties, key=lambda p:len(str(p["party"])))["party"])))
            elif i == 1:
                headers[i][1] = max(len(headers[i][0]), len(str(max(self.parties, key=lambda p:len(str(p["password"])))["password"])))
            elif i == len(headers)-1: # last column is for the average
                headers[i][1] = max(len(headers[i][0]), len(str("DPS: {:,}".format(max(self.parties, key=lambda p:len(str(p["average"])))["average"]))))
            else:               # players are a combination of string
                                # and numbers converted to string with
                                # commas as magnitude separator, that
                                # is, ign, dps and level
                ignMaxSize = len(str("IGN: {}".format(max(self.parties, key=lambda p:len(str(p["members"][i-2].ign)))["members"][i-2].ign)))
                dpsMaxSize = len(str("DPS: {:,}".format(max(self.parties, key=lambda p:len(str(p["members"][i-2].dps)))["members"][i-2].dps)))
                levelMaxSize = len(str("Level: {:,}".format(max(self.parties, key=lambda p:len(str(p["members"][i-2].level)))["members"][i-2].level)))
                headers[i][1] = max(len(headers[i][0]), ignMaxSize, dpsMaxSize, levelMaxSize)
            rowSeparator += "-"*(headers[i][1]+(padding*2))
            rowSeparator += "+"
        rowSeparator += "\n"
        
        
        table += rowSeparator
        
        for i in range(0, len(headers)):
            table += "|" + (" "*padding) + headers[i][0] + (" "*(padding + (headers[i][1] - len(str(headers[i][0])))))
        table += "|\n"
        table += rowSeparator
        
        
        
        for p in self.parties:
            # each cell has 3 rows. for party name, password and
            # average, only the second row is used. for party members
            # there is the ign, the dps and the level, one for each
            # row.
            
            # row 0
            for i in range(0, len(headers)):
                if i <= 1 or i == len(headers) - 1:      # party, password and average rows are empty
                    table += "|{0}{fill}{0}".format(" "*padding, fill=" "*(headers[i][1]))
                else:           # party member
                    table += "|{0}IGN: {fill}{1}{0}".format(" "*padding, p["members"][i-2].ign, fill=" "*(headers[i][1] - len(p["members"][i-2].ign) - len("IGN: ")))
            table += "|\n"
            
            # row 1
            for i in range(0, len(headers)):
                if i == 0:      # party name
                    table += "|{0}{1}{fill}{0}".format(" "*padding, p["party"], fill=" "*(headers[i][1] - len(p["party"])))
                elif i == 1:    # password
                    table += "|{0}{1}{fill}{0}".format(" "*padding, p["password"], fill=" "*(headers[i][1] - len(p["password"])))
                elif i == len(headers) - 1:    # average
                    average = "{:,}".format(p["average"])
                    table += "|{0}DPS: {fill}{1}{0}".format(" "*padding, average, fill=" "*(headers[i][1] - len(average) - len("DPS: ")))
                else:           # party member
                    dps = "{:,}".format(p["members"][i-2].dps)
                    table += "|{0}DPS: {fill}{1}{0}".format(" "*padding, dps, fill=" "*(headers[i][1] - len(dps) - len("DPS: ")))
            table += "|\n"
            
            # row 2
            for i in range(0, len(headers)):
                if i <= 1 or i == len(headers) - 1:      # party, password and average rows are empty
                    table += "|{0}{fill}{0}".format(" "*padding, fill=" "*(headers[i][1]))
                else:           # party member
                    level = "{:,}".format(p["members"][i-2].level)
                    table += "|{0}Level: {fill}{1}{0}".format(" "*padding, level, fill=" "*(headers[i][1] - len(level) -len("Level: ")))
            table += "|\n"
            table += rowSeparator

        return table
    
    
    def getTableSpreadsheetMode(self):
        """Makes the table in plain text for the spreadsheet, with tabs as
        field separators and new lines as rows
        
        @return                                 string containing the table
        
        """
        
        table = ""
        for p in self.parties:
            fields = [p["party"]]
            for m in p["members"]:
                if m.ign == "Unknown":
                    fields.append("None")
                else:
                    fields.append(m.ign)
            table += "{}\n".format("\t".join(fields))
        
        return table

    
    
    def printSummary(self):
        """
        Prints a summary for each player in the table
        """
        
        for m in self.members:
            m.printSummary()
            print("-"*80)
    
    

if __name__ == "__main__":
    
    table = PartyTable()
    
    table.addParty("Alpha", [
        {
            "ign": "brutalboy",
            "dps": 123456789,
            "level": 123
        },
        {
            "ign": "turidrum",
            "dps": 123456,
            "level": 12
        },
        {
            "ign": "brutalboy",
            "dps": 123456789,
            "level": 123
        },
        {
            "ign": "turidrum",
            "dps": 123456,
            "level": 12
        }
    ])
    
    print(table.getTableTextMode())
    print(table.getTableSpreadsheetMode())
