#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#
# ------------------------------------------------------------------------------
#
#   PartyMaker - Python version of the old PartyMaker with export for pastebin
#   Copyright (C) 2018 turidrum
#
#   This program is free software; you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation; either version 2 of the
#   License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
#   02110-1301, USA.
#
# ------------------------------------------------------------------------------




class SubscriptionList:
    
    def __init__(self):
        """Constructor

        """
        self.members = []


    def parse(self, text):
        """Parses a text as copied from the subscription list, and makes a
        list of members from it

        @param text                             the text as coipied from the subscription list
        @return                                 a list of members

        """
        self.members = []

        # each row represents a member, each member property is
        # described as a key value pair separated by semicolomns, for
        # example key = value;
        for row in text.split("\n"):
            if row.strip(" ") != "":
                rawdata = [ x.strip(" ") for x in row.split(";")]
                member = {
                    "ign": "",
                    "yc": 0,
                    "status": "clean",
                    "reason": "",
                    "role": "normal",
                    "subscribed": "yes",
                    "spreadsheet": "ok",
                    "info": "",
                    "forceIn": "no",
                    "forceDps": "",
                    "forceLevel": ""
                }
                for pair in rawdata:
                    if len(pair.split("=")) == 2: 
                        key = pair.split("=")[0].strip(" ")
                        val = pair.split("=")[1].strip(" ")
                        member[key] = val
                self.members.append(member)
        return self.members

    def update(self, members=[]):
        self.members = members

    def makeList(self):
        """Makes a subscription list

        @return                                 string representing the subscription list
        """

        text = ""
        for m in self.members:
            text += "ign={};".format(m["ign"])
            if m["subscribed"] != "yes":
                text += " subscribed={};".format(m["subscribed"])
            if m["role"] != "normal":
                text += " role={};".format(m["role"])
            if m["status"] != "clean":
                text += " status={};".format(m["status"])
            if int(m["yc"]) > 0:
                text += " yc={};".format(m["yc"])
            if m["reason"] != "":
                text += " reason={};".format(m["reason"])
            if m["spreadsheet"] != "ok":
                text += " spreadsheet={};".format(m["spreadsheet"])
            if m["info"] != "":
                text += " info={};".format(m["info"])
            if m["forceIn"] != "no":
                text += " forceIn={};".format(m["forceIn"])
            if m["forceDps"] != "":
                text += " forceDps={};".format(m["forceDps"])
            if m["forceLevel"] != "":
                text += " forceLevel={};".format(m["forceLevel"])
            text += "\n"
        return text

if __name__ == "__main__":
    from character import Character
    sub = SubscriptionList()

    text = """ign=OmegaMan5000; yc=2; status=doing the first penalty session; reason=joined too late;
ign=Modeon;
ign=brutalboy;
ign=brutal's-alt-1; role=filler;
ign = brutal's-alt-2; role = filler; info = priority on recruiting, in parties only if both guilds are full and without inactives
ign=Damador;
ign=bmerich33;
ign=Timonius;
ign=Lyve; spreadsheet=column too old;
ign=Baga; spreadsheet=column too old;
ign=xDx; yc=2; status=1 session clean; reason=joined too late; spreadsheet=column too old;
ign=anonymoous; spreadsheet=column too old;
ign=Jelle; yc=2; status=1 session clean; reason=joined too late; spreadsheet=column too old;
ign=Rdbj;
ign=Faaaaaart; yc=2; status=1 session clean; reason=joined too late;
ign=dmwro;
ign=daninin; spreadsheet=column too old;
ign=OldBarbon; spreadsheet=column too old;
ign=DanyX94; spreadsheet=column too old;
ign=Xfxforce;
ign=Aperikub;
ign=Tiopa;
ign=Acmano179;
ign=monoxxide;
ign=Jumpz;
ign=simbu;
ign=locker01;
ign=aethnight;
ign=iforgiveyou;
ign=QContinuum;
ign=xBlo;
ign=Iluya;
    """
    data = sub.parse(text)
    for s in data:
        m = Character(
            s["ign"],
            yc = s["yc"],
            ycStatus = s["ycStatus"],
            partyRole = s["partyRole"],
            subscribed = s["subscribed"],
            spreadsheetStatus = s["spreadsheetStatus"],
            info = s["info"]
        )
        m.printSummary()
        print("-"*80)
