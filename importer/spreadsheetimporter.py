#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#
# ------------------------------------------------------------------------------
#
#   PartyMaker - Python version of the old PartyMaker with export for pastebin
#   Copyright (C) 2018 turidrum
#
#   This program is free software; you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation; either version 2 of the
#   License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
#   02110-1301, USA.
#
# ------------------------------------------------------------------------------


import urllib.parse
import urllib.request
import csv

class SpreadsheetImporter:
    def __init__(self, url):
        """Constructor

        """
        
        self.url = url          # link to the spreadsheet
        
        
    def fetch(self):
        """
        Fetches the data from the spreadsheet and return a string as if it were manually copied selecting the cells

        @return                                 the text as if it were copied from the spreadsheet
        """
        req = urllib.request.Request(self.url)
        names = []
        updates = ""
        dpses = []
        levels = []
        text = ""
        with urllib.request.urlopen(req) as response:
            csvData = response.read().decode("utf-8").split("\r\n")
            dialect = csv.Sniffer().sniff(csvData[1]) # getting the dialect from the update row is efficient because is the one with most of the complexity
            names = csvData[0].split(",")[1:]
            updates = "".join([x[0] if x[0] != "" else "#" for x in list(csv.reader(csvData[1], dialect))]).split("#")[1:]
            dpses = "".join([x[0] if x[0] != "" else "#" for x in list(csv.reader(csvData[2], dialect))]).split("#")[1:]
            levels = csvData[3].split(",")[1:]

        
        text += "\t".join(names)
        text += "\n"
        text += "\t".join(updates)
        text += "\n"
        text += "\t".join(dpses)
        text += "\n"
        text += "\t".join(levels)
        text += "\n"
        
        
        return text


if __name__ == "__main__":
    si = SpreadsheetImporter("")
    r = si.fetch()
    print(r)
    
