#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#
# ------------------------------------------------------------------------------
#
#   PartyMaker - Python version of the old PartyMaker with export for pastebin
#   Copyright (C) 2018 turidrum
#
#   This program is free software; you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation; either version 2 of the
#   License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
#   02110-1301, USA.
#
# ------------------------------------------------------------------------------


import datetime

class SpreadsheetInput:
    def __init__(self):
        """Constructor

        """
        self.members = []


    def parse(self, text):
        """Parses a text as copied from the spreadsheet and makes a list of
        members from it
        
        @param text                             the text as copied from the spreadsheet
        @return                                 a list of members

        """

        self.members = []

        # the spreadsheet has 4 rows, ign, last update, dps, level,
        # and each column represents a member. cells are separated by
        # tabs, and rows by new lines
        igns = [x for x in (text.split("\n")[0].split("\t"))]
        updates = [x for x in (text.split("\n")[1].split("\t"))]
        dpses = [x for x in (text.split("\n")[2].split("\t"))]
        levels = [x for x in (text.split("\n")[3].split("\t"))]
        
        for i in range(0, len(igns)):
            member = {
                "ign": igns[i],
                "dps": dpses[i],
                "level": levels[i],
                "update": updates[i],
                "spreadsheetStatus": "ok"
            }
            if member["ign"] != "": # there are some empty or abandoned columns
                self.members.append(member)
        return self.members

if __name__ == "__main__":
    from character import Character
    sheet = SpreadsheetInput()
    
    text = """brutalboy	Jelle	Tidlz	AkuneAntok	Tiopa	OmegaMan5000	Lyve	charfellow	gooshy	Kaillion	Overon	Gwondonolas	Mariblez	Modeon	FigoWigo	FalseHopes	SARASAY	bmerich33		SnowWhite	Gotrek			Mc0wn	SuperDavid			Unquenchable		Darina	Cairo	xDx	Aethnight	Aeric	Faaaaaart		Fruit		Iluya	MrTom	dmwro	QContinuum	Coldsewoo	gwango	gwang	Ysayell	anonymoous	monoxxide	Baga	Joep	Rdbj	mummoo	ant292	Kelzan	aperikub	Clarisse62	rety	CharlieBMF		Damador			Aerow	Darkon	Trepotri	simbu		Gismoh	Bibbolicious		bbtoskar	Whalor				brutalgirl				Timonius									Wildcolt			daninin	OldBarbon	DanyX94		Xerox	Locker01				cxvxx			Rinnegan				xBlo	Acmano179	IIIIIIIIIIIII		Krovax	Iforgiveyou	Nakratash	heatseeker	Xfxforce		Jumpz	Xincredible	Reps		None	brutal's-alt-1	brutal's-alt-2	
07/16, 12:19 PM	07/16, 02:24 PM	04/24, 06:54 AM	06/05, 08:49 PM	07/15, 03:29 PM	07/20, 10:12 PM	07/22, 10:19 PM	07/15, 09:35 PM	06/05, 08:56 PM	06/07, 09:13 AM	07/23, 05:42 AM	06/05, 08:56 PM	07/23, 05:42 AM	07/23, 06:27 AM	07/03, 02:51 PM	07/14, 07:21 PM	07/20, 03:23 AM	07/16, 01:13 PM	07/13, 05:08 PM	04/30, 03:21 AM	05/28, 12:52 PM	07/13, 05:07 PM	07/13, 05:07 PM	05/07, 08:36 AM	06/11, 03:02 AM	07/13, 05:05 PM	07/13, 05:06 PM	07/13, 05:05 PM	07/13, 05:05 PM	06/11, 10:27 AM	07/13, 05:12 PM	07/23, 07:25 AM	07/23, 05:32 AM	06/30, 11:31 AM	07/22, 11:05 PM	07/13, 04:56 PM	04/16, 05:34 AM	07/13, 04:57 PM	07/23, 05:39 AM	06/25, 05:43 AM	07/22, 11:46 PM	07/23, 06:55 AM	07/23, 01:57 AM	07/22, 04:16 AM	07/22, 04:22 AM	07/23, 07:52 AM	06/30, 10:57 AM	07/23, 04:31 AM	07/02, 10:03 AM	07/22, 11:09 PM	07/23, 05:29 AM	04/23, 12:54 PM	05/02, 08:17 PM	06/18, 10:06 PM	07/23, 06:38 AM	07/05, 04:43 AM	06/20, 12:24 PM	07/05, 04:43 AM	07/13, 04:55 PM	07/16, 09:29 AM	07/13, 05:03 PM	07/13, 05:02 PM	06/10, 11:43 PM	05/31, 06:21 PM	05/24, 04:32 PM	07/23, 07:16 AM	06/18, 02:17 PM	07/23, 12:48 AM	07/22, 07:20 PM	03/06, 02:45 PM	06/25, 04:42 AM	04/30, 03:24 PM	07/13, 05:02 PM	07/13, 05:02 PM	05/13, 02:10 PM	07/02, 01:50 AM	02/16, 03:10 PM	05/13, 02:10 PM	02/16, 03:10 PM	07/09, 07:49 AM	03/29, 12:42 PM	03/01, 06:26 PM	03/01, 06:26 PM	03/01, 06:26 PM	07/13, 05:00 PM	07/13, 04:59 PM	07/13, 04:59 PM	07/13, 04:59 PM	06/10, 07:40 PM	07/13, 04:59 PM	07/13, 04:59 PM	07/02, 11:15 AM	06/25, 11:32 AM	06/25, 11:56 AM	07/13, 04:59 PM	06/24, 09:47 PM	07/23, 07:23 AM	06/11, 09:03 PM	12/18, 07:29 PM	04/12, 02:29 PM	04/12, 04:09 PM	05/13, 02:12 PM	07/13, 05:11 PM	05/24, 04:44 PM	05/13, 02:13 PM	05/13, 02:13 PM	07/07, 10:33 AM	07/23, 07:46 AM	07/16, 08:54 AM	06/09, 10:22 PM	07/13, 05:00 PM	07/01, 08:39 PM	07/21, 08:40 PM	07/09, 11:43 PM	04/22, 08:51 PM	07/22, 12:22 PM	07/13, 05:00 PM	07/23, 04:41 AM	07/17, 09:10 PM	06/23, 11:00 AM					
269,769,838,968,948	25,445,805,278,116	75,846,408,755	1,976,758,406,664	12,665,429,009	64,423,706,669,719	9,047,436,805,728	18,880,686,269	12,562,886,348,324	152,501,627,962,564	2,239,154,221	10,052,525,559,388	2,289,127,355	46,918,832,653,318	59,336,629,207,413	1,973,296,583,883	4,108,786,827,141	39,240,464,657,479	8	129,434,195,493	79,357,532,152	8	8	55,154,790,916	5,596,470,600,729	8	8	6,703,396,477	8	4,054,896,068,102	17,565,917	140,321,588,477,632	1,804,414,756	33,222,810,090,503	22,436,194,283,730	8	588,293,009,330	8	132,167,466,798	240,374,761,346	151,203,450,598,713	1,179,262,626	37,498,123,773	1,771,106,698	11,525,348	371,106,884,962	207,481,282,684,677	68,131,251,418,491	34,886,776,542,516	5,396,800,213	13,170,295,903,752	823,543,849,607	252,548,013,081	15,432,287,687,602	902,524,813,411	28,766,242,327,023	1,726,973,989,633	27,036,428,496,336	8	28,281,416,377,527	8	8	4,857,931,770,373	2,246,358,298	7,018,924,090,869	312,576,853	8	466,695,845	68,298,643	8	53,815,662,121,756	244,390,856,069	8	8	8	75,978	8	8	8	58,464,394,524,378	8	8	8	8	8	8	8	8	538,376,614,337	8	8	1,650,069,384,820	13,401,764,565	179,852,381	8	649,153,534,110	17,234,267,394,780	8	8	8	565,974,646,531	8	8	126,000,232,074	8	8	8	1,075,439,163	663,286,920,496	704,765,420,774	8	1,754,759,178	2,322,105,405,725	14,938,788,689	408,988,426,558	282,682,597,855	8	3,377,424,176,115	1,759,588,695	1,710,470,536		0	9,000,000,000,000	8,500,000,000,000	1,000,000,000
1956	1669	1177	1291	646	1718	1467	717	1473	1881	497	1460	537	1750	1602	1230	1323	1741	0	1082	921	0	0	1197	1192	0	0	765	0	1532	1128	1915	522	1637	1659	0	1152	0	886	1064	1939	501	738	508	275	1087	1913	1758	1553	545	1521	1380	1168	1435	1166	1620	1318	1614	0	1564	0	0	1288	718	1385	585	0	433	373	0	1741	1233	0	0	0	209	0	0	0	1719	0	0	0	0	0	0	0	0	1179	0	0	1245	769	538	0	1181	1458	0	0	0	1124	0	0	751	0	0	0	494	1193	1125	0	507	1245	586	1208	1104	0	1256	631	492		0	1321	1317	"""
    data = sheet.parse(text)
    for s in data:
        print(s)
        print("-"*80)
