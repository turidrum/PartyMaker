#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#
# ------------------------------------------------------------------------------
#
#   PartyMaker - Python version of the old PartyMaker with export for pastebin
#   Copyright (C) 2018 turidrum
#
#   This program is free software; you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation; either version 2 of the
#   License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
#   02110-1301, USA.
#
# ------------------------------------------------------------------------------


import re

class Character:
    def __init__(self, ign, level=0, dps=0, yc=0, ycStatus=0, ycReason="", partyRole="normal", subscribed="yes", spreadsheetStatus="ok", info=""):
        self.ign = ign
        self.level = int("".join(re.findall("[0-9]", str(level))))
        self.dps = int("".join(re.findall("[0-9]", str(dps))))
        self.yc = int(yc)
        self.ycStatus = ycStatus
        self.ycReason = ycReason
        self.partyRole = partyRole
        self.subscribed = subscribed
        self.spreadsheetStatus = spreadsheetStatus
        self.info = info
        
        
    def printSummary(self):
        """
        print a summary for this character
        """

        if self.ign != "":
            print("IGN: {:<20}".format(self.ign))
        if self.level > 0:
            print("Level: {:<20,}".format(self.level))
        if self.dps > 0:
            print("dps: {:<20,}".format(self.dps))
        if self.yc > 0:
            print("Yellow Cards: {:<20,}".format(self.yc))
        if self.ycStatus != "clean":
            print("YC Status: {:<20}".format(self.ycStatus))
        if self.ycReason != "":
            print("YC Reason: {:<20}".format(self.ycReason))
        if self.subscribed != "yes":
            print("Subscribed: {:<20}".format(self.subscribed))
        if self.partyRole != "normal":
            print("Party Role: {:<20}".format(self.partyRole))
        if self.spreadsheetStatus != "ok":
            print("Spreadsheet Status: {:<20}".format(self.spreadsheetStatus))
        if self.otherInfo != "":
            print("Other Info: {:<20}".format(self.otherInfo))
        
        

if __name__ == "__main__":
    brutalboy = Character("brutalboy", 1954, 391933, 3, "clean since 2 sessions", "filler")
    brutalboy.printSummary()
    

